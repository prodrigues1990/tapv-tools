﻿using System.Threading.Tasks;
using TapVTools.Core.Entities;
using TapVTools.Core.Interfaces.Repositories;

namespace MemoryInfrastructure.Repositories
{
    public class FlightRepository : IFlightRepository
    {
        public CurrentFlight currentFlight;

        public FlightRepository()
        {
            currentFlight = new CurrentFlight()
            {
                AircraftRegistration = "CS-TNP",
                ArrivalICAO = "LPPR",
                DepartureICAO = "LPPT"
            };
        }

        public Task<CurrentFlight> GetCurrentFlight()
        {
            if (currentFlight == null)
                throw new NoFlightAssigned();
            return Task.FromResult(currentFlight);
        }

        public Task ResetCurrentFlight()
        {
            currentFlight = null;
            return Task.CompletedTask;
        }
    }
}
