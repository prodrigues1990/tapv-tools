﻿using System.Threading.Tasks;
using TapVTools.Core.Entities;
using TapVTools.Core.Interfaces.Repositories;

namespace MemoryInfrastructure.Repositories
{
    public class UserRepository : IUserRepository
    {
        private User currentUser = null;

        public Task<User> GetCurrentUser()
        {
            return Task.FromResult(currentUser);
        }

        public Task SignIn(string callsign, string password)
        {
            if (callsign != password)
                throw new WrongCredentials();
            return Task.CompletedTask;
        }
    }
}
