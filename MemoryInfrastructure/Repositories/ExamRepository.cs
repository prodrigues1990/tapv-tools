﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TapVTools.Core.Entities;
using TapVTools.Core.Interfaces.Repositories;

namespace MemoryInfrastructure.Repositories
{
    public class ExamRepository : IExamRepository
    {
        private List<ExamQuestion> questions = new List<ExamQuestion>()
        {
            new ExamQuestion() { Question = "Question 1", Answer = "Answer 1", CorrectedAnswerSelected = true },
            new ExamQuestion() { Question = "Question 2", Answer = "Answer 2" , CorrectedAnswerSelected = false}
        };

        public Task<List<ExamQuestion>> GetCurrentExamQuestions()
        {
            return Task.FromResult(questions);
        }
    }
}
