﻿using Autofac;
using MemoryInfrastructure.Repositories;
using TapVTools.Core.Interfaces.Repositories;

namespace MemoryInfrastructure
{
    public class MemoryInfrastructureModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<UserRepository>()
                .As<IUserRepository>()
                .SingleInstance();
            builder.RegisterType<FlightRepository>()
                .As<IFlightRepository>()
                .InstancePerLifetimeScope();
            builder.RegisterType<ExamRepository>()
                .As<IExamRepository>()
                .InstancePerLifetimeScope();
        }
    }
}
