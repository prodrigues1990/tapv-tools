

## DevOps scripts

### Version Bump

Automatically update all version tags.

```bash
find . -regextype posix-extended -regex '^.*\.(cs|wxs)' -exec sed -i -e 's/<current_version>/<new_version>/g' {} \;
```