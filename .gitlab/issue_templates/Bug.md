## Description

_Provide any contextual information the issue, screen where it happened, any special
conditions on the TAPv FOS, etc._


## Expected Behaviour

_Describe the behaviour you expected from the application._


## Actual Behaviour

_What did the application actually did._


## Steps to Reproduce the Problem

  1.
  1.
  1.


## Log file

_Drag and drop in here the `TapVTools.log` file located in the program directory._