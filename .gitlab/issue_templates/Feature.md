## Behaviour

_The side effects to be done, by the proposed feature, in TAPv FOS._


## Interface

_Information and actions the user should do to interact with the application in the
scope of the proposed feature._


## Presentation

_What and how relevant should be displayed to the user._

