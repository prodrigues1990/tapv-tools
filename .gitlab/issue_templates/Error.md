## Description

_Provide any contextual information of the actions you took before this error,
screen where it happened, any special conditions on the TAPv FOS, etc._


## Expected Behaviour

_What did you expected the applicaton to do before this error._


## Actual Behaviour

The application crashed.


## Log file

_Drag and drop in here the `TapVTools.log` file located in the program directory._


## Steps to Reproduce the Problem

  1.
  1.
  1.
