﻿using System.Threading.Tasks;
using TapVTools.Core.Dto.UseCaseRequests;
using TapVTools.Core.Dto.UseCaseResponses;
using TapVTools.Core.Interfaces;
using TapVTools.Core.Interfaces.Repositories;
using TapVTools.Core.Interfaces.UseCases;

namespace TapVTools.Core.UseCases
{
    public class GetExamAnswersUseCase : IGetExamAnswersUseCase
    {
        private const int NextExecutionDeferTime = 10 * 1000;

        private IExamRepository examRepository;

        public GetExamAnswersUseCase(IExamRepository examRepository)
        {
            this.examRepository = examRepository;
        }

        public async Task Handle(
            GetExamAnswersUseCaseRequest message,
            IOutputPort<GetExamAnswersUseCaseResponse> outputPort)
        {
            outputPort.Handle(new GetExamAnswersUseCaseResponse()
            { Questions = await examRepository.GetCurrentExamQuestions() });

            await Task.Delay(NextExecutionDeferTime);
        }
    }
}
