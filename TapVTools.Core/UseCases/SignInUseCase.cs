﻿using System.Threading.Tasks;
using TapVTools.Core.Dto.UseCaseRequests;
using TapVTools.Core.Dto.UseCaseResponses;
using TapVTools.Core.Interfaces;
using TapVTools.Core.Interfaces.Repositories;
using TapVTools.Core.Interfaces.UseCases;

namespace TapVTools.Core.UseCases
{
    public class SignInUseCase : ISignInUseCase
    {
        private IUserRepository userRepository;

        public SignInUseCase(IUserRepository userRepository)
        {
            this.userRepository = userRepository;
        }

        public async Task Handle(
            SignInUseCaseRequest message,
            IOutputPort<SignInUseCaseResponse> outputPort)
        {
            await userRepository.SignIn(message.Callsign, message.Password);
            
            outputPort.Handle(new SignInUseCaseResponse()
                { User = await userRepository.GetCurrentUser() });
        }
    }
}
