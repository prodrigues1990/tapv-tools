﻿using System.Threading.Tasks;
using TapVTools.Core.Dto.UseCaseRequests;
using TapVTools.Core.Dto.UseCaseResponses;
using TapVTools.Core.Interfaces;
using TapVTools.Core.Interfaces.Repositories;
using TapVTools.Core.Interfaces.UseCases;

namespace TapVTools.Core.UseCases
{
    public class GetCurrentFlightUseCase : IGetCurrentFlightUseCase
    {
        private IFlightRepository flightRepository;

        public GetCurrentFlightUseCase(IFlightRepository flightRepository)
        {
            this.flightRepository = flightRepository;
        }

        public async Task Handle(
            GetCurrentFlightUseCaseRequest message,
            IOutputPort<GetCurrentFlightUseCaseResponse> outputPort)
        {
            outputPort.Handle(new GetCurrentFlightUseCaseResponse()
                { CurrentFlight = await flightRepository.GetCurrentFlight() });
        }
    }
}
