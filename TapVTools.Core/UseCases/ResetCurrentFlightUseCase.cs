﻿using System;
using System.Threading.Tasks;
using TapVTools.Core.Dto.UseCaseRequests;
using TapVTools.Core.Dto.UseCaseResponses;
using TapVTools.Core.Interfaces;
using TapVTools.Core.Interfaces.Repositories;
using TapVTools.Core.Interfaces.UseCases;

namespace TapVTools.Core.UseCases
{
    public class ResetCurrentFlightUseCase : IResetCurrentFlightUseCase
    {
        private IFlightRepository flightRepository;

        public ResetCurrentFlightUseCase(IFlightRepository flightRepository)
        {
            this.flightRepository = flightRepository;
        }

        public async Task Handle(
            ResetCurrentFlightUseCaseRequest message,
            IOutputPort<ResetCurrentFlightUseCaseResponse> outputPort)
        {
            await flightRepository.ResetCurrentFlight();

            DateTime now = DateTime.Now;
            TimeSpan step = TimeSpan.FromMinutes(5);
            DateTime resetingAt = new DateTime(
                (now.Ticks + step.Ticks - 1) / step.Ticks * step.Ticks, now.Kind);

            outputPort.Handle(new ResetCurrentFlightUseCaseResponse()
            { ResetingAt = resetingAt });
        }
    }
}
