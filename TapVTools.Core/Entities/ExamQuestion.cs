﻿namespace TapVTools.Core.Entities
{
    public class ExamQuestion
    {
        public string Question { get; set; }

        public string Answer { get; set; }

        public bool CorrectedAnswerSelected { get; set; }
    }
}
