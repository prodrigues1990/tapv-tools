﻿namespace TapVTools.Core.Entities
{
    public class CurrentFlight
    {
        public string AircraftRegistration { get; set; }

        public string DepartureICAO { get; set; }

        public string ArrivalICAO { get; set; }
    }
}
