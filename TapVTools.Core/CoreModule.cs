﻿using Autofac;
using TapVTools.Core.Interfaces.UseCases;
using TapVTools.Core.UseCases;

namespace TapVTools.Core
{
    public class CoreModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<SignInUseCase>()
                .As<ISignInUseCase>()
                .SingleInstance();
            builder.RegisterType<GetCurrentFlightUseCase>()
                .As<IGetCurrentFlightUseCase>()
                .SingleInstance();
            builder.RegisterType<ResetCurrentFlightUseCase>()
                .As<IResetCurrentFlightUseCase>()
                .SingleInstance();
            builder.RegisterType<GetExamAnswersUseCase>()
                .As<IGetExamAnswersUseCase>()
                .SingleInstance();
        }
    }
}
