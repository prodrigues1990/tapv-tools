﻿using TapVTools.Core.Dto.UseCaseResponses;
using TapVTools.Core.Interfaces;

namespace TapVTools.Core.Dto.UseCaseRequests
{
    public class GetExamAnswersUseCaseRequest :
        IUseCaseRequest<GetExamAnswersUseCaseResponse>
    {
    }
}
