﻿using TapVTools.Core.Dto.UseCaseResponses;
using TapVTools.Core.Interfaces;

namespace TapVTools.Core.Dto.UseCaseRequests
{
    public class SignInUseCaseRequest : IUseCaseRequest<SignInUseCaseResponse>
    {
        public string Callsign { get; set; }

        public string Password { get; set; }
    }
}
