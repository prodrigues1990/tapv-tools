﻿using System;
using TapVTools.Core.Interfaces;

namespace TapVTools.Core.Dto.UseCaseResponses
{
    public class ResetCurrentFlightUseCaseResponse : IUseCaseResponse
    {
        public DateTime ResetingAt { get; set; }
    }
}