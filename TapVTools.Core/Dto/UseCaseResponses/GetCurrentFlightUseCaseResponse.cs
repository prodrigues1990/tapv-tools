﻿using TapVTools.Core.Entities;
using TapVTools.Core.Interfaces;

namespace TapVTools.Core.Dto.UseCaseResponses
{
    public class GetCurrentFlightUseCaseResponse : IUseCaseResponse
    {
        public CurrentFlight CurrentFlight;
    }
}
