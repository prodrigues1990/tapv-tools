﻿using TapVTools.Core.Entities;
using TapVTools.Core.Interfaces;

namespace TapVTools.Core.Dto.UseCaseResponses
{
    public class SignInUseCaseResponse : IUseCaseResponse
    {
        public User User { get; internal set; }
    }
}
