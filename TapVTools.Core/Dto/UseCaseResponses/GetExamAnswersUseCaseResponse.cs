﻿using System.Collections.Generic;
using TapVTools.Core.Entities;
using TapVTools.Core.Interfaces;

namespace TapVTools.Core.Dto.UseCaseResponses
{
    public class GetExamAnswersUseCaseResponse : IUseCaseResponse
    {
        public List<ExamQuestion> Questions { get; set; }
    }
}
