﻿using System.Threading.Tasks;

namespace TapVTools.Core.Interfaces
{
    public interface IUseCaseRequestHandler<in TUseCaseRequest, out TUseCaseResponse> where TUseCaseRequest : IUseCaseRequest<TUseCaseResponse>
    {
        Task Handle(TUseCaseRequest message, IOutputPort<TUseCaseResponse> outputPort);
    }
}
