﻿using TapVTools.Core.Dto.UseCaseRequests;
using TapVTools.Core.Dto.UseCaseResponses;

namespace TapVTools.Core.Interfaces.UseCases
{
    public interface ISignInUseCase :
        IUseCaseRequestHandler<SignInUseCaseRequest, SignInUseCaseResponse>
    { }
}
