﻿using System;
using System.Threading.Tasks;
using TapVTools.Core.Entities;

namespace TapVTools.Core.Interfaces.Repositories
{
    public interface IFlightRepository
    {
        Task<CurrentFlight> GetCurrentFlight();
        Task ResetCurrentFlight();
    }

    public class NoFlightAssigned : Exception { }
}