﻿using System;
using System.Threading.Tasks;
using TapVTools.Core.Entities;

namespace TapVTools.Core.Interfaces.Repositories
{
    public interface IUserRepository
    {
        Task SignIn(string callsign, string password);
        Task<User> GetCurrentUser();
    }

    public class NotSignedIn : Exception { }
    public class WrongCredentials : Exception { }
}
