﻿using MySql.Data.MySqlClient;

namespace TapVTools.Core.Interfaces.Repositories
{
    public interface IMySqlProvider
    {
        MySqlConnection getConnection();
    }
}
