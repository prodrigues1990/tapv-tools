﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TapVTools.Core.Entities;

namespace TapVTools.Core.Interfaces.Repositories
{
    public interface IExamRepository
    {
        Task<List<ExamQuestion>> GetCurrentExamQuestions();
    }
}
