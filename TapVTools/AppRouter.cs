﻿using System.Windows.Forms;
using TapVTools.Core.Entities;
using TapVTools.Models.Interfaces;
using TapVTools.Views;

namespace TapVTools
{
    public class AppRouter : ApplicationContext
    {
        private SignInForm signInForm;
        private MainForm mainForm;

        public AppRouter(ISignInModel signInModel, IMainModel mainModel)
        {
            signInModel.OnUserChanged += onUserChanged;
            signInForm = new SignInForm(signInModel);
            signInForm.FormClosed += SignInForm_FormClosed;

            mainForm = new MainForm(mainModel);
            mainForm.FormClosed += MainForm_FormClosed;

            MainForm = signInForm;
            signInForm.Show();
        }

        private void SignInForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (!mainForm.Visible)
                ExitThread();
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            ExitThread();
        }

        private void onUserChanged(object sender, User user)
        {
            if (signInForm.Visible)
            {
                MainForm = mainForm;
                mainForm.Show();
                signInForm.Close();
            }
        }
    }
}
