﻿using System.ComponentModel;
using System.Diagnostics;
using TapVTools.Legacy;
using TapVTools.Models.Interfaces;
using TapVTools.Properties;
using TapVTools.Views;

namespace TapVTools.Models
{
    public class MainModel : IMainModel
    {
        public string FeatureRequestUrl =>
            @"https://gitlab.com/prodrigues1990/tapv-tools/issues/new?issuable_template=Feature";

        public string FlyTapvWebsiteUrl => @"http://flytapv.com";

        public string BugReportUrl =>
            @"https://gitlab.com/prodrigues1990/tapv-tools/issues/new?issuable_template=Bug";

        public string FosUrl => @"http://asa-virtual.org/tapv/fos";

        
        public ExamQuestionsForm ExamQuestionsForm { get; private set; }

        public CurrentFlightForm CurrentFlightForm { get; private set; }

        private FlightMonitorForm flightMonitorForm;
        public FlightMonitorForm FlightMonitorForm
        {
            get
            {
                if (flightMonitorForm == null)
                    flightMonitorForm = new FlightMonitorForm();
                return flightMonitorForm;
            }
        }

        public MainModel(
            ICurrentFlightModel currentFlightModel,
            IExamQuestionsModel examQuestionsModel)
        {
            CurrentFlightForm = new CurrentFlightForm(currentFlightModel);
            ExamQuestionsForm = new ExamQuestionsForm(examQuestionsModel);
        }

        public void OpenTapLog()
        {
            Process proc = new Process();

            proc.StartInfo.FileName = string.Format("{0}\\{1}",
                Settings.Default.TAPLogInstallFolder,
                "TAPLog.exe");
            proc.StartInfo.WorkingDirectory = Settings.Default.TAPLogInstallFolder;
            proc.StartInfo.UseShellExecute = true;
            proc.StartInfo.Verb = "runas";
            try { proc.Start(); }
            catch (Win32Exception error)
            {
                if (error.Message != "The operation was canceled by the user")
                    throw error;
            }
        }
    }
}
