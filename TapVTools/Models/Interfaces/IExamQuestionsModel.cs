﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TapVTools.Core.Entities;

namespace TapVTools.Models.Interfaces
{
    public interface IExamQuestionsModel
    {
        event EventHandler<List<ExamQuestion>> OnQuestionsChanged;

        Task GetQuestions();
    }
}
