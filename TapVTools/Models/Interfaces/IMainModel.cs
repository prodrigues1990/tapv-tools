﻿using TapVTools.Legacy;
using TapVTools.Views;

namespace TapVTools.Models.Interfaces
{
    public interface IMainModel
    {
        string FlyTapvWebsiteUrl { get; }
        string BugReportUrl { get; }
        string FeatureRequestUrl { get; }
        ExamQuestionsForm ExamQuestionsForm { get; }
        CurrentFlightForm CurrentFlightForm { get; }
        string FosUrl { get; }
        FlightMonitorForm FlightMonitorForm { get; }

        void OpenTapLog();
    }
}
