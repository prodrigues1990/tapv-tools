﻿using System;
using TapVTools.Core.Entities;

namespace TapVTools.Models.Interfaces
{
    public interface ICurrentFlightModel
    {
        event EventHandler<CurrentFlight> OnCurrentFlightChanged;
        event EventHandler<DateTime> OnCurrentFlightReseted;
        event EventHandler<Exception> OnError;

        void RefreshCurrentFlight();
        void ResetCurrentFlight();
    }
}
