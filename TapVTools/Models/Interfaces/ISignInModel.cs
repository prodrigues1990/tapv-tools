﻿using System;
using TapVTools.Core.Entities;

namespace TapVTools.Models.Interfaces
{
    public interface ISignInModel
    {
        event EventHandler<User> OnUserChanged;
        event EventHandler<Exception> OnError;

        SignInSettings GetSavedSignInSettings();
        void SignIn(SignInSettings signInSettings);
    }

    public class SignInSettings
    {
        public SignInSettings(string callsign, string password, bool remember)
        {
            Callsign = callsign;
            Password = password;
            Remember = remember;
        }

        public string Callsign { get; set; }
        public string Password { get; set; }
        public bool Remember { get; set; }
    }
}
