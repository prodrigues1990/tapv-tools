﻿using AutoMapper;
using System;
using TapVTools.Core.Dto.UseCaseRequests;
using TapVTools.Core.Dto.UseCaseResponses;
using TapVTools.Core.Entities;
using TapVTools.Core.Interfaces;
using TapVTools.Core.Interfaces.UseCases;
using TapVTools.Models.Interfaces;
using TapVTools.Properties;

namespace TapVTools.Models
{
    public class SignInModel : ISignInModel, IOutputPort<SignInUseCaseResponse>
    {
        private IMapper mapper;
        private ISignInUseCase signInUseCase;
        
        public event EventHandler<User> OnUserChanged;
        public event EventHandler<Exception> OnError;

        public SignInModel(IMapper mapper, ISignInUseCase signInUseCase)
        {
            this.mapper = mapper;
            this.signInUseCase = signInUseCase;
        }

        public void Handle(SignInUseCaseResponse response)
        {
            if (Settings.Default.Remember)
                Settings.Default.Save();

            OnUserChanged?.Invoke(this, response.User);
        }

        public async void SignIn(SignInSettings signInSettings)
        {
            Settings.Default.Callsign = signInSettings.Callsign;
            Settings.Default.Password = signInSettings.Password;
            Settings.Default.Remember = signInSettings.Remember;

            SignInUseCaseRequest request = new SignInUseCaseRequest()
            {
                Callsign = signInSettings.Callsign,
                Password = signInSettings.Password
            };

            try
            {
                await signInUseCase.Handle(request, this);
            }
            catch (Exception error)
            {
                Settings.Default.Reload();

                if (OnError == null)
                    throw error;
                else
                    OnError(this, error);
            }
        }

        public SignInSettings GetSavedSignInSettings()
        {
            return mapper.Map<SignInSettings>(Settings.Default);
        }
    }
}
