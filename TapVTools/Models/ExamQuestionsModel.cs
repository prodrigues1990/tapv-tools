﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TapVTools.Core.Dto.UseCaseRequests;
using TapVTools.Core.Dto.UseCaseResponses;
using TapVTools.Core.Entities;
using TapVTools.Core.Interfaces;
using TapVTools.Core.Interfaces.UseCases;
using TapVTools.Models.Interfaces;

namespace TapVTools.Models
{
    public class ExamQuestionsModel : IExamQuestionsModel, IOutputPort<GetExamAnswersUseCaseResponse>
    {
        private IGetExamAnswersUseCase getExamAnswersUseCase;

        public ExamQuestionsModel(IGetExamAnswersUseCase getExamAnswersUseCase)
        {
            this.getExamAnswersUseCase = getExamAnswersUseCase;
        }

        public event EventHandler<List<ExamQuestion>> OnQuestionsChanged;

        public Task GetQuestions()
        {
            return getExamAnswersUseCase
                .Handle(new GetExamAnswersUseCaseRequest(), this);
        }

        public void Handle(GetExamAnswersUseCaseResponse response)
        {
            OnQuestionsChanged?.Invoke(this, response.Questions);
        }
    }
}
