﻿using System;
using System.Threading.Tasks;
using TapVTools.Core.Dto.UseCaseRequests;
using TapVTools.Core.Dto.UseCaseResponses;
using TapVTools.Core.Entities;
using TapVTools.Core.Interfaces;
using TapVTools.Core.Interfaces.UseCases;
using TapVTools.Models.Interfaces;

namespace TapVTools.Models
{
    public class CurrentFlightModel :
        ICurrentFlightModel,
        IOutputPort<GetCurrentFlightUseCaseResponse>,
        IOutputPort<ResetCurrentFlightUseCaseResponse>
    {
        private IGetCurrentFlightUseCase getCurrentFlightUseCase;
        private IResetCurrentFlightUseCase resetCurrentFlightUseCase;

        public event EventHandler<CurrentFlight> OnCurrentFlightChanged;
        public event EventHandler<DateTime> OnCurrentFlightReseted;
        public event EventHandler<Exception> OnError;

        public CurrentFlightModel(
            IGetCurrentFlightUseCase getCurrentFlightUseCase,
            IResetCurrentFlightUseCase resetCurrentFlightUseCase)
        {
            this.getCurrentFlightUseCase = getCurrentFlightUseCase;
            this.resetCurrentFlightUseCase = resetCurrentFlightUseCase;
        }

        public async void RefreshCurrentFlight()
        {
            GetCurrentFlightUseCaseRequest request =
                new GetCurrentFlightUseCaseRequest();
            try
            {
                await getCurrentFlightUseCase.Handle(request, this);
            }
            catch (Exception error)
            {
                if (OnError == null)
                    throw error;
                else
                    OnError(this, error);
            }
        }

        public async void ResetCurrentFlight()
        {
            ResetCurrentFlightUseCaseRequest request =
                new ResetCurrentFlightUseCaseRequest();
            try
            {
                await resetCurrentFlightUseCase.Handle(request, this);
            }
            catch (Exception error)
            {
                if (OnError == null)
                    throw error;
                else
                    OnError(this, error);
            }
        }

        public void Handle(GetCurrentFlightUseCaseResponse response)
        {
            OnCurrentFlightChanged?.Invoke(this, response.CurrentFlight);
        }

        public async void Handle(ResetCurrentFlightUseCaseResponse response)
        {
            OnCurrentFlightReseted?.Invoke(this, response.ResetingAt);

            await Task.Delay(response.ResetingAt - DateTime.Now);
            RefreshCurrentFlight();
        }
    }
}
