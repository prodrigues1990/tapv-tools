﻿using Autofac;
using AutoMapper;
using TapVTools.Models.Interfaces;
using TapVTools.Properties;

namespace TapVTools
{
    public class ApplicationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var assembly = System.Reflection.Assembly.GetExecutingAssembly();

            Mapper modelMapper = new Mapper(new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Settings, SignInSettings>();
            }));
            builder.RegisterInstance(modelMapper).As<IMapper>();

            // Register MVP Models
            builder.RegisterAssemblyTypes(assembly)
               .Where(t => t.Name.EndsWith("Model"))
               .AsImplementedInterfaces();

            // Register main application router
            builder.RegisterType<AppRouter>().As<AppRouter>().SingleInstance();
        }
    }
}
