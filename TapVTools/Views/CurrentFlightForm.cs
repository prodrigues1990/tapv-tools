﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using TapVTools.Core.Entities;
using TapVTools.Core.Interfaces.Repositories;
using TapVTools.Models.Interfaces;
using TapVTools.Presenters;
using TapVTools.Views.Interfaces;
using WeifenLuo.WinFormsUI.Docking;

namespace TapVTools.Views
{
    public partial class CurrentFlightForm : DockContent, ICurrentFlightView
    {
        CurrentFlightPresenter presenter;

        bool flashTaskbar = false;

        [DllImport("user32.dll")]
        static extern bool FlashWindow(IntPtr hwnd, bool bInvert);

        public CurrentFlightForm(ICurrentFlightModel model)
        {
            presenter = new CurrentFlightPresenter(this, model);
            model.OnError += onError;

            InitializeComponent();

            Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath);
        }

        public CurrentFlight CurrentFlight
        {
            set
            {
                txtCurrentFlight.Text = string.Format("{0} {1}-{2}",
                    value.AircraftRegistration,
                    value.DepartureICAO,
                    value.ArrivalICAO);
                btnRefresh.Enabled = true;
                btnReset.Enabled = true;
            }
        }

        public DateTime ResetingAt
        {
            set
            {
                txtCurrentFlight.Text = string.Format("Reseting at {0}",
                    value.ToShortTimeString());
                btnRefresh.Enabled = false;
                btnReset.Enabled = false;
            }
        }

        private void CurrentFlightForm_Load(object sender, EventArgs e)
        {
            btnRefresh.Enabled = true;
            btnReset.Enabled = false;

            presenter.ViewLoaded();
        }

        private void onError(object sender, Exception error)
        {
            btnRefresh.Enabled = true;
            btnReset.Enabled = true;

            try
            {
                throw error;
            }
            catch (NoFlightAssigned)
            {
                if (flashTaskbar)
                    FlashWindow(Handle, true);
                flashTaskbar = false;
                txtCurrentFlight.Text = "No flight assigned.";
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            btnReset.Enabled = false;
            flashTaskbar = true;

            presenter.ResetCurrentFlight();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            btnRefresh.Enabled = false;

            presenter.RefreshCurrentFlight();
        }
    }
}
