﻿namespace TapVTools.Views
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.lblSoftwareVersion = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.flightToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnOpenFlightReset = new System.Windows.Forms.ToolStripMenuItem();
            this.btnOpenFlightMonitor = new System.Windows.Forms.ToolStripMenuItem();
            this.tapVirtualToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnOpenFos = new System.Windows.Forms.ToolStripMenuItem();
            this.btnOpenWebsite = new System.Windows.Forms.ToolStripMenuItem();
            this.btnOpenTapLog = new System.Windows.Forms.ToolStripMenuItem();
            this.examsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnOpenExamAnswers = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnReportBug = new System.Windows.Forms.ToolStripMenuItem();
            this.btnRequestFeature = new System.Windows.Forms.ToolStripMenuItem();
            this.dockPanel = new WeifenLuo.WinFormsUI.Docking.DockPanel();
            this.statusStrip.SuspendLayout();
            this.menuStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblSoftwareVersion});
            this.statusStrip.Location = new System.Drawing.Point(0, 321);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(612, 22);
            this.statusStrip.TabIndex = 0;
            this.statusStrip.Text = "statusStrip1";
            // 
            // lblSoftwareVersion
            // 
            this.lblSoftwareVersion.Name = "lblSoftwareVersion";
            this.lblSoftwareVersion.Size = new System.Drawing.Size(0, 17);
            // 
            // menuStrip2
            // 
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.flightToolStripMenuItem,
            this.tapVirtualToolStripMenuItem,
            this.examsToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menuStrip2.Location = new System.Drawing.Point(0, 0);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.Size = new System.Drawing.Size(612, 24);
            this.menuStrip2.TabIndex = 2;
            this.menuStrip2.Text = "menuStrip2";
            // 
            // flightToolStripMenuItem
            // 
            this.flightToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnOpenFlightReset,
            this.btnOpenFlightMonitor});
            this.flightToolStripMenuItem.Name = "flightToolStripMenuItem";
            this.flightToolStripMenuItem.Size = new System.Drawing.Size(49, 20);
            this.flightToolStripMenuItem.Text = "Flight";
            // 
            // btnOpenFlightReset
            // 
            this.btnOpenFlightReset.Name = "btnOpenFlightReset";
            this.btnOpenFlightReset.Size = new System.Drawing.Size(154, 22);
            this.btnOpenFlightReset.Text = "Reset Current...";
            this.btnOpenFlightReset.Click += new System.EventHandler(this.btnOpenFlightReset_Click);
            // 
            // btnOpenFlightMonitor
            // 
            this.btnOpenFlightMonitor.Name = "btnOpenFlightMonitor";
            this.btnOpenFlightMonitor.Size = new System.Drawing.Size(154, 22);
            this.btnOpenFlightMonitor.Text = "Flight Monitor";
            this.btnOpenFlightMonitor.Click += new System.EventHandler(this.btnOpenFlightMonitor_Click);
            // 
            // tapVirtualToolStripMenuItem
            // 
            this.tapVirtualToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnOpenFos,
            this.btnOpenWebsite,
            this.btnOpenTapLog});
            this.tapVirtualToolStripMenuItem.Name = "tapVirtualToolStripMenuItem";
            this.tapVirtualToolStripMenuItem.Size = new System.Drawing.Size(75, 20);
            this.tapVirtualToolStripMenuItem.Text = "Tap Virtual";
            // 
            // btnOpenFos
            // 
            this.btnOpenFos.Name = "btnOpenFos";
            this.btnOpenFos.Size = new System.Drawing.Size(181, 22);
            this.btnOpenFos.Text = "Open FOS Website...";
            this.btnOpenFos.Click += new System.EventHandler(this.btnOpenFos_Click);
            // 
            // btnOpenWebsite
            // 
            this.btnOpenWebsite.Name = "btnOpenWebsite";
            this.btnOpenWebsite.Size = new System.Drawing.Size(181, 22);
            this.btnOpenWebsite.Text = "Open flytapv.com...";
            this.btnOpenWebsite.Click += new System.EventHandler(this.btnOpenWebsite_Click);
            // 
            // btnOpenTapLog
            // 
            this.btnOpenTapLog.Name = "btnOpenTapLog";
            this.btnOpenTapLog.Size = new System.Drawing.Size(181, 22);
            this.btnOpenTapLog.Text = "Open TAPLog...";
            this.btnOpenTapLog.Click += new System.EventHandler(this.btnOpenTapLog_Click);
            // 
            // examsToolStripMenuItem
            // 
            this.examsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnOpenExamAnswers});
            this.examsToolStripMenuItem.Name = "examsToolStripMenuItem";
            this.examsToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.examsToolStripMenuItem.Text = "Exams";
            // 
            // btnOpenExamAnswers
            // 
            this.btnOpenExamAnswers.Name = "btnOpenExamAnswers";
            this.btnOpenExamAnswers.Size = new System.Drawing.Size(131, 22);
            this.btnOpenExamAnswers.Text = "My Exam...";
            this.btnOpenExamAnswers.Click += new System.EventHandler(this.btnOpenExamAnswers_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnReportBug,
            this.btnRequestFeature});
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.aboutToolStripMenuItem.Text = "About";
            // 
            // btnReportBug
            // 
            this.btnReportBug.Name = "btnReportBug";
            this.btnReportBug.Size = new System.Drawing.Size(176, 22);
            this.btnReportBug.Text = "Report a Bug...";
            this.btnReportBug.Click += new System.EventHandler(this.btnReportBug_Click);
            // 
            // btnRequestFeature
            // 
            this.btnRequestFeature.Name = "btnRequestFeature";
            this.btnRequestFeature.Size = new System.Drawing.Size(176, 22);
            this.btnRequestFeature.Text = "Request a Feature...";
            this.btnRequestFeature.Click += new System.EventHandler(this.btnRequestFeature_Click);
            // 
            // dockPanel
            // 
            this.dockPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dockPanel.Location = new System.Drawing.Point(0, 24);
            this.dockPanel.Name = "dockPanel";
            this.dockPanel.Size = new System.Drawing.Size(612, 297);
            this.dockPanel.TabIndex = 3;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(612, 343);
            this.Controls.Add(this.dockPanel);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.menuStrip2);
            this.IsMdiContainer = true;
            this.Name = "MainForm";
            this.Text = "Tap Virtual Tools";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.ToolStripMenuItem flightToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem btnOpenFlightReset;
        private System.Windows.Forms.ToolStripMenuItem examsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem btnOpenExamAnswers;
        private WeifenLuo.WinFormsUI.Docking.DockPanel dockPanel;
        private System.Windows.Forms.ToolStripStatusLabel lblSoftwareVersion;
        private System.Windows.Forms.ToolStripMenuItem tapVirtualToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem btnOpenFos;
        private System.Windows.Forms.ToolStripMenuItem btnOpenWebsite;
        private System.Windows.Forms.ToolStripMenuItem btnOpenTapLog;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem btnReportBug;
        private System.Windows.Forms.ToolStripMenuItem btnRequestFeature;
        private System.Windows.Forms.ToolStripMenuItem btnOpenFlightMonitor;
    }
}