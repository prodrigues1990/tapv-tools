﻿using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using TapVTools.Core.Entities;
using TapVTools.Models.Interfaces;
using TapVTools.Presenters;
using TapVTools.Views.Interfaces;
using WeifenLuo.WinFormsUI.Docking;

namespace TapVTools.Views
{
    public partial class ExamQuestionsForm : DockContent, IExamQuestionsView
    {
        ExamQuestionsPresenter presenter;

        public ExamQuestionsForm(IExamQuestionsModel model)
        {
            InitializeComponent();

            presenter = new ExamQuestionsPresenter(this, model);

            Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath);
        }

        public List<ExamQuestion> Questions
        {
            set
            {
                lst.Items.Clear();
                foreach (ExamQuestion question in value) {
                    lst.Items.Add(new ListViewItem(new string[]
                        { question.Question, question.Answer }));
                    if (question.CorrectedAnswerSelected)
                    {
                        lst.Items[lst.Items.Count - 1].BackColor = Color.White;
                        lst.Items[lst.Items.Count - 1].ForeColor = Color.Green;
                    }
                    else
                    {
                        lst.Items[lst.Items.Count - 1].BackColor = Color.Red;
                        lst.Items[lst.Items.Count - 1].ForeColor = Color.White;
                    }
                }
            }
        }

        private async void ExamQuestionsForm_Load(object sender, System.EventArgs e)
        {
            await presenter.ViewLoaded();
            btnRefresh.Enabled = true;
        }

        private async void btnRefresh_Click(object sender, System.EventArgs e)
        {
            btnRefresh.Enabled = false;
            await presenter.Refresh();
            btnRefresh.Enabled = true;
        }
    }
}
