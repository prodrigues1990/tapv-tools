﻿using System;
using TapVTools.Core.Entities;

namespace TapVTools.Views.Interfaces
{
    public interface ICurrentFlightView
    {
        CurrentFlight CurrentFlight { set; }

        DateTime ResetingAt { set; }
    }
}
