﻿namespace TapVTools.Views.Interfaces
{
    public interface IMainView
    {
        string SoftwareVersion { set; }
    }
}
