﻿namespace TapVTools.Views.Interfaces
{
    public interface ISignInView
    {
        string Callsign { get; set; }
        string Password { get; set; }
        bool Remember { get; set; }
    }
}
