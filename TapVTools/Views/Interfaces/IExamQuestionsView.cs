﻿using System.Collections.Generic;
using TapVTools.Core.Entities;

namespace TapVTools.Views.Interfaces
{
    public interface IExamQuestionsView
    {
        List<ExamQuestion> Questions { set; }
    }
}
