﻿using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using TapVTools.Models.Interfaces;
using TapVTools.Presenters;
using TapVTools.Views.Interfaces;
using WeifenLuo.WinFormsUI.Docking;

namespace TapVTools.Views
{
    public partial class MainForm : Form, IMainView
    {
        private MainPresenter presenter;

        public string SoftwareVersion { set { lblSoftwareVersion.Text = value; } }

        public MainForm(IMainModel model)
        {
            InitializeComponent();

            presenter = new MainPresenter(this, model);

            Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath);
            presenter.ViewLoaded(Application.ProductVersion);
        }

        private void btnOpenFlightReset_Click(object sender, System.EventArgs e)
        {
            presenter.CurrentFlightForm.Show(dockPanel, DockState.Document);
        }

        private void MainForm_Load(object sender, System.EventArgs e)
        {
            presenter.CurrentFlightForm.Show(dockPanel, DockState.Document);
        }

        private void btnOpenExamAnswers_Click(object sender, System.EventArgs e)
        {
            presenter.ExamQuestionsForm.Show(dockPanel, DockState.Document);
        }

        private void btnOpenFos_Click(object sender, System.EventArgs e)
        {
            Process.Start(presenter.FosUrl);
        }

        private void btnOpenWebsite_Click(object sender, System.EventArgs e)
        {
            Process.Start(presenter.FlyTapvWebsiteUrl);
        }

        private void btnOpenTapLog_Click(object sender, System.EventArgs e)
        {
            presenter.OpenTapLog();
        }

        private void btnReportBug_Click(object sender, System.EventArgs e)
        {
            Process.Start(presenter.BugReportUrl);
        }

        private void btnRequestFeature_Click(object sender, System.EventArgs e)
        {
            Process.Start(presenter.FeatureRequestUrl);
        }

        private void btnOpenFlightMonitor_Click(object sender, System.EventArgs e)
        {
            presenter.FlightMonitorForm.Show(dockPanel, DockState.Document);
        }
    }
}
