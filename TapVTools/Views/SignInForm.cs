﻿using System;
using System.Drawing;
using System.Windows.Forms;
using TapVTools.Core.Interfaces.Repositories;
using TapVTools.Models.Interfaces;
using TapVTools.Presenters;
using TapVTools.Views.Interfaces;

namespace TapVTools.Views
{
    public partial class SignInForm : Form, ISignInView
    {
        SignInPresenter presenter;

        public string Callsign
        {
            get => txtCallsign.Text;
            set => txtCallsign.Text = value;
        }

        public string Password
        {
            get => txtPassword.Text;
            set => txtPassword.Text = value;
        }
        
        public bool Remember
        {
            get => chkRemember.Checked;
            set => chkRemember.Checked = value;
        }

        public SignInForm(ISignInModel model)
        {
            presenter = new SignInPresenter(this, model);
            model.OnError += onError;

            InitializeComponent();

            Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath);
        }

        private void btnSignIn_Click(object sender, EventArgs e)
        {
            btnSignIn.Enabled = false;
            btnSignIn.Text = "Wait...";

            presenter.SignIn();
        }

        private void txt_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnSignIn_Click(sender, e);
        }

        private void onError(object sender, Exception error)
        {
            try
            {
                throw error;
            }
            catch (WrongCredentials)
            {
                lblStatus.Text = "Wrong callsign or password.";
                lblStatus.ForeColor = Color.DarkRed;
            }
            finally
            {
                btnSignIn.Enabled = true;
                btnSignIn.Text = "Sign In";
            }
        }

        private void SignInForm_Load(object sender, EventArgs e)
        {
            btnSignIn.Enabled = true;
            btnSignIn.Text = "Sign In";

            presenter.ViewLoaded();
        }
    }
}
