﻿using FSUIPC;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using TapVTools.Properties;
using WeifenLuo.WinFormsUI.Docking;

namespace TapVTools.Legacy
{
    public partial class FlightMonitorForm : DockContent
    {
        string flightPhase;
        string OfpAircraft;
        string OfpFlightNumber;
        string OfpDeparture;
        string OfpArrival;
        int Score = 100;
        int Weight;
        long LastPosition = 0;
        DirectoryInfo dirLogs;
        FileInfo filCurrent;
        Timer tmrFolder = new Timer();
        Timer tmrFile = new Timer();
        static public Offset<double> grossWeight = new Offset<double>(0x30C0);
        Dictionary<string, Event> EventList = new Dictionary<string, Event>();
        Dictionary<string, int> TakeoffWeights = new Dictionary<string, int>();
        Dictionary<string, int> LandingWeights = new Dictionary<string, int>();

        public FlightMonitorForm()
        {
            InitializeComponent();
            Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath);

            EventList.Add("1_2", new Event("Vmo/Mmo Exceeded", 20, true));
            EventList.Add("3A", new Event("Flap / Slat Placard Speed Exceeded", 15, true));
            EventList.Add("3F", new Event("Landing Gear Down Limit (IAS) Exceeded", 20, true));
            EventList.Add("3G", new Event("Landing Gear Down Limit (Mach) Exceeded", 20, true));
            EventList.Add("3H", new Event("Landing gear limit (extension)", 20, true));
            EventList.Add("3I", new Event("Landing gear limit (retraction)", 15, true));
            EventList.Add("4", new Event("Flap Altitude limit Exceeded", 15, true));
            EventList.Add("5", new Event("Max. Operating Altitude Exceeded", 35, true));
            EventList.Add("6A", new Event("Approach Speed High", 5, true));
            EventList.Add("6B", new Event("Approach Speed High below 500ft AGL", 5, true));
            EventList.Add("6C", new Event("Approach Speed High below 50ft AGL", 5, true));
            EventList.Add("7A", new Event("Approach Speed Low below 1000ft AGL", 5, true));
            EventList.Add("8A", new Event("Climb out speed high below 400ft AAL", 5, true));
            EventList.Add("8B", new Event("Climb out speed high 400ft to 1000ft AAL", 5, true));
            EventList.Add("10A", new Event("Unstick speed high", 5, true));
            EventList.Add("10B", new Event("Unstick speed low", 5, false));
            EventList.Add("10C", new Event("Tire Speed Limit Exceeded", 20, true));
            EventList.Add("20A", new Event("Pitch High during TO", 10, true));
            EventList.Add("20B", new Event("Pitch High on LDG", 10, true));
            EventList.Add("21A", new Event("Excessive Bank 20ft to 100ft AAL", 10, true));
            EventList.Add("21B", new Event("Excessive Bank 100ft to 500ft AAL", 10, true));
            EventList.Add("21C", new Event("Excessive Bank above 500ft AAL", 10, true));
            EventList.Add("22A", new Event("Descent rate High below 400ft AAL", 10, true));
            EventList.Add("22B", new Event("Descent rate High 1000ft to 400ft AAL", 5, true));
            EventList.Add("22C", new Event("Descent rate High 2000ft to1000ft AAL", 5, true));
            EventList.Add("22F", new Event("Slow climb to 1000AAL after TO", 5, true));
            EventList.Add("23A", new Event("falsermal G on GND", 5, false));
            EventList.Add("23B", new Event("falsermal G on Flight", 5, true));
            EventList.Add("23C", new Event("Hard Landing", 20, true));
            EventList.Add("26", new Event("Abandoned TO", 5, true));
            EventList.Add("41", new Event("Power change on APP", 5, true));
            EventList.Add("48B", new Event("Incorrect Land Flap", 20, false));
            EventList.Add("56A", new Event("Below G/S 600ft to 150ft AAL", 10, true));
            EventList.Add("56B", new Event("Above G/S 600ft to 150ft AAL", 10, true));
            EventList.Add("57", new Event("MTOW Exceeded", 35, true));
            EventList.Add("58", new Event("MLW Exceeded", 35, true));
            EventList.Add("59", new Event("Fuel not match", 5, false));
            EventList.Add("60", new Event("Speedbrakes on Landing Config", 10, false));
            EventList.Add("61", new Event("Alternate Airport", 5, true));
            EventList.Add("62", new Event("Inconsistent departure time", 5, true));
            EventList.Add("63", new Event("Offline Flight", 4, true));

            TakeoffWeights.Add("319", 68000);
            LandingWeights.Add("319", 61000);
            TakeoffWeights.Add("320", 77000);
            LandingWeights.Add("320", 64500);
            TakeoffWeights.Add("332", 233000);
            LandingWeights.Add("332", 182000);
            TakeoffWeights.Add("333", 233000);
            LandingWeights.Add("333", 187000);
            TakeoffWeights.Add("343", 257000);
            LandingWeights.Add("343", 188000);
            
            dirLogs = new DirectoryInfo(Path.Combine(Settings.Default.TAPLogInstallFolder, "logs"));

            tmrFolder.Interval = 1000;
            tmrFile.Interval = 1000;

            tmrFolder.Tick += TmrFolder_Tick;
            tmrFile.Tick += TmrFile_Tick;

            tmrFolder.Start();
        }

        private void TmrFile_Tick(object sender, EventArgs e)
        {
            using (var file = File.Open(filCurrent.FullName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                if (LastPosition == file.Length)
                    return;

                file.Position = LastPosition;
                using (var reader = new StreamReader(file))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        if (OfpAircraft == null)
                            OfpAircraft = CaptureAircraft(line);
                        if (OfpArrival == null)
                            OfpArrival = CaptureArrival(line);
                        if (OfpDeparture == null)
                            OfpDeparture = CaptureDeparture(line);
                        if (OfpFlightNumber == null)
                            OfpFlightNumber = CaptureFlightNumber(line);

                        flightPhase = CaptureFlightPhase(line);
                        CaptureEvent(line);
                    }
                    LastPosition = file.Position; // Store this somewhere too.
                }
            }


                Weight = CaptureWeight();

            if (flightPhase != null)
                switch (flightPhase)
                {
                    case "TTO":
                        // Monitor takeoff weight
                        if (OfpAircraft != null && Weight > TakeoffWeights[OfpAircraft])
                        {
                            txtWeightWarningText.Text = "Overweight";
                            txtWeightWarning.Text = String.Format("{0} kg over", Weight - TakeoffWeights[OfpAircraft]);
                        }
                        break;
                    case "TO":
                        if (OfpAircraft != null && Weight > TakeoffWeights[OfpAircraft])
                        {
                            // call attention
                            WindowState = FormWindowState.Normal;
                            Activate();
                        }
                        break;
                    case "CRS":
                    case "DES":
                        // Monitor landing weight
                        if (OfpAircraft != null && Weight > LandingWeights[OfpAircraft])
                        {
                            txtWeightWarningText.Text = "Overweight";
                            txtWeightWarning.Text = String.Format("{0} kg over", Weight - LandingWeights[OfpAircraft]);
                        }
                        break;
                    case "APP":
                        if (OfpAircraft != null && Weight > LandingWeights[OfpAircraft])
                        {
                            // call attention
                            WindowState = FormWindowState.Normal;
                            Activate();
                        }
                        break;
                    default:
                        txtWeightWarningText.Text = "";
                        txtWeightWarning.Text = "";
                        break;
                }

            txtGrossWeight.Text = (Weight > 0) ? Weight.ToString() + " kg" : "";
            txtAircraft.Text = (OfpAircraft != null && OfpFlightNumber != null) ? String.Format("{0} A{1}", OfpFlightNumber, OfpAircraft) : "";
            txtRoute.Text = (OfpArrival != null && OfpDeparture != null) ? String.Format("{0} - {1}", OfpDeparture, OfpArrival) : "";
        }

        public string CaptureFlightPhase(string line)
        {
            Regex regex = new Regex(@"Fase\sde\svoo\s\([\d]\)\:\s(TTO|TO|CLB|CRS|DES|APR|LND|ROL|TL)");
            Match match = regex.Match(line);
            if (match.Success)
                return match.Groups[1].Value;

            return null;
        }

        private void AddEventLine(string time, string code)
        {
            Event e = null;
            if (EventList.ContainsKey(code))
            {
                e = EventList[code];
            }

            lstEvents.Items.Add((e != null) ? String.Format("{0}: {1}.{2}", time, code, e.Description)
                                            : String.Format("{0}: {1}.unknown event", time, code));

            if (e == null || e.Active)
            {
                Score -= e.Discount;
                txtScore.Text = Score.ToString() + "%";

                // call attention
                WindowState = FormWindowState.Normal;
                Activate();
            }
        }

        public string CaptureAircraft(string line)
        {
            Regex regex = new Regex(@"OFP\saircraft\s-\s([\d]{3})");
            Match match = regex.Match(line);
            if (match.Success)
                return match.Groups[1].Value;

            return null;
        }

        public string CaptureFlightNumber(string line)
        {
            Regex regex = new Regex(@"OFP\scodflight\s-\s(TP[\d]{3})");
            Match match = regex.Match(line);
            if (match.Success)
                return match.Groups[1].Value;

            return null;
        }

        public string CaptureDeparture(string line)
        {
            Regex regex = new Regex(@"OFP\sDeparture\s-\s([A-Z]{4})");
            Match match = regex.Match(line);
            if (match.Success)
                return match.Groups[1].Value;

            return null;
        }

        public string CaptureArrival(string line)
        {
            Regex regex = new Regex(@"OFP\sArrival\s-\s([A-Z]{4})");
            Match match = regex.Match(line);
            if (match.Success)
                return match.Groups[1].Value;

            return null;
        }

        public string CaptureEvent(string line)
        {
            Regex regex = new Regex(@"^([\d]{2}/[\d]{2}/[\d]{4}\s[\d]{2}\:[\d]{2}\:[\d]{2})\s-\sEvento:\s(.+?)\:");
            Match match = regex.Match(line);
            if (match.Success)
            {
                AddEventLine(match.Groups[1].Value, match.Groups[2].Value);
                return match.Groups[2].Value;
            }

            return null;
        }

        public int CaptureWeight()
        {
            try
            {
                FSUIPCConnection.Open();
                FSUIPCConnection.Process();
            }
            catch (Exception e)
            {
                return -1;
            }

            double weight = grossWeight.Value * 0.45359237;

            FSUIPCConnection.Close();

            return (int)weight;
        }

        private void TmrFolder_Tick(object sender, EventArgs e)
        {
            FileInfo latestFile = dirLogs
                .GetFiles()
                .OrderByDescending(f => f.LastWriteTime)
                .First();

            if (filCurrent == null || filCurrent.FullName != latestFile.FullName)
            {
                // new file, start monitoring this one
                flightPhase = null;
                OfpAircraft = null;
                OfpArrival = null;
                OfpDeparture = null;
                OfpFlightNumber = null;
                Weight = -1;
                filCurrent = latestFile;
                LastPosition = 0;
                Score = 100;
                lstEvents.Items.Clear();
                tmrFile.Start();

                txtScore.Text = Score.ToString() + "%";
                txtStatus.Text = "Monitoring " + filCurrent.Name;
            }

        }
    }

    public class Event
    {
        public string Description;
        public int Discount;
        public bool Active;

        public Event(string Description, int Discount, bool Active)
        {
            this.Description = Description;
            this.Discount = Discount;
            this.Active = Active;
        }
    }
}
