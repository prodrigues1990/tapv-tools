﻿namespace TapVTools.Legacy
{
    partial class FlightMonitorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstEvents = new System.Windows.Forms.ListBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.txtStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.txtScore = new System.Windows.Forms.TextBox();
            this.txtAircraft = new System.Windows.Forms.TextBox();
            this.txtRoute = new System.Windows.Forms.TextBox();
            this.txtGrossWeight = new System.Windows.Forms.TextBox();
            this.txtWeightWarning = new System.Windows.Forms.TextBox();
            this.txtWeightWarningText = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lstEvents
            // 
            this.lstEvents.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lstEvents.FormattingEnabled = true;
            this.lstEvents.Location = new System.Drawing.Point(12, 12);
            this.lstEvents.Name = "lstEvents";
            this.lstEvents.Size = new System.Drawing.Size(426, 225);
            this.lstEvents.TabIndex = 3;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.txtStatus});
            this.statusStrip1.Location = new System.Drawing.Point(0, 247);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(554, 22);
            this.statusStrip1.TabIndex = 4;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // txtStatus
            // 
            this.txtStatus.Name = "txtStatus";
            this.txtStatus.Size = new System.Drawing.Size(0, 17);
            // 
            // txtScore
            // 
            this.txtScore.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtScore.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtScore.Location = new System.Drawing.Point(444, 214);
            this.txtScore.Multiline = true;
            this.txtScore.Name = "txtScore";
            this.txtScore.ReadOnly = true;
            this.txtScore.Size = new System.Drawing.Size(98, 20);
            this.txtScore.TabIndex = 6;
            this.txtScore.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtAircraft
            // 
            this.txtAircraft.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAircraft.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtAircraft.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAircraft.Location = new System.Drawing.Point(444, 38);
            this.txtAircraft.Name = "txtAircraft";
            this.txtAircraft.ReadOnly = true;
            this.txtAircraft.Size = new System.Drawing.Size(98, 15);
            this.txtAircraft.TabIndex = 6;
            this.txtAircraft.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtRoute
            // 
            this.txtRoute.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtRoute.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtRoute.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRoute.Location = new System.Drawing.Point(444, 59);
            this.txtRoute.Name = "txtRoute";
            this.txtRoute.ReadOnly = true;
            this.txtRoute.Size = new System.Drawing.Size(98, 14);
            this.txtRoute.TabIndex = 6;
            this.txtRoute.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtGrossWeight
            // 
            this.txtGrossWeight.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtGrossWeight.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtGrossWeight.Location = new System.Drawing.Point(444, 170);
            this.txtGrossWeight.Multiline = true;
            this.txtGrossWeight.Name = "txtGrossWeight";
            this.txtGrossWeight.ReadOnly = true;
            this.txtGrossWeight.Size = new System.Drawing.Size(98, 20);
            this.txtGrossWeight.TabIndex = 6;
            this.txtGrossWeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtWeightWarning
            // 
            this.txtWeightWarning.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtWeightWarning.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtWeightWarning.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWeightWarning.ForeColor = System.Drawing.Color.Maroon;
            this.txtWeightWarning.Location = new System.Drawing.Point(444, 126);
            this.txtWeightWarning.Multiline = true;
            this.txtWeightWarning.Name = "txtWeightWarning";
            this.txtWeightWarning.ReadOnly = true;
            this.txtWeightWarning.Size = new System.Drawing.Size(98, 20);
            this.txtWeightWarning.TabIndex = 6;
            this.txtWeightWarning.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtWeightWarningText
            // 
            this.txtWeightWarningText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtWeightWarningText.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtWeightWarningText.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWeightWarningText.ForeColor = System.Drawing.Color.Maroon;
            this.txtWeightWarningText.Location = new System.Drawing.Point(444, 108);
            this.txtWeightWarningText.Multiline = true;
            this.txtWeightWarningText.Name = "txtWeightWarningText";
            this.txtWeightWarningText.ReadOnly = true;
            this.txtWeightWarningText.Size = new System.Drawing.Size(98, 20);
            this.txtWeightWarningText.TabIndex = 6;
            this.txtWeightWarningText.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.ForeColor = System.Drawing.Color.Maroon;
            this.textBox1.Location = new System.Drawing.Point(444, 196);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(98, 20);
            this.textBox1.TabIndex = 6;
            this.textBox1.Text = "Expected Score";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox2
            // 
            this.textBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.ForeColor = System.Drawing.Color.Maroon;
            this.textBox2.Location = new System.Drawing.Point(444, 152);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(98, 20);
            this.textBox2.TabIndex = 6;
            this.textBox2.Text = "Gross Weight";
            this.textBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // FlightMonitorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(554, 269);
            this.Controls.Add(this.txtRoute);
            this.Controls.Add(this.txtAircraft);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.txtWeightWarningText);
            this.Controls.Add(this.txtWeightWarning);
            this.Controls.Add(this.txtGrossWeight);
            this.Controls.Add(this.txtScore);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.lstEvents);
            this.HideOnClose = true;
            this.MinimumSize = new System.Drawing.Size(500, 160);
            this.Name = "FlightMonitorForm";
            this.Text = "Flight Monitor";
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ListBox lstEvents;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel txtStatus;
        private System.Windows.Forms.TextBox txtScore;
        private System.Windows.Forms.TextBox txtAircraft;
        private System.Windows.Forms.TextBox txtRoute;
        private System.Windows.Forms.TextBox txtGrossWeight;
        private System.Windows.Forms.TextBox txtWeightWarning;
        private System.Windows.Forms.TextBox txtWeightWarningText;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
    }
}

