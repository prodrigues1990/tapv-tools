﻿using System;
using TapVTools.Core.Entities;
using TapVTools.Models.Interfaces;
using TapVTools.Views.Interfaces;

namespace TapVTools.Presenters
{
    public class CurrentFlightPresenter
    {
        private readonly ICurrentFlightView view;
        private readonly ICurrentFlightModel model;

        public CurrentFlightPresenter(ICurrentFlightView view, ICurrentFlightModel model)
        {
            model.OnCurrentFlightChanged += Model_OnCurrentFlightChanged;
            model.OnCurrentFlightReseted += Model_OnCurrentFlightReseted;

            this.view = view;
            this.model = model;
        }

        private void Model_OnCurrentFlightReseted(object sender, DateTime e)
        {
            view.ResetingAt = e;
        }

        private void Model_OnCurrentFlightChanged(object sender, CurrentFlight e)
        {
            view.CurrentFlight = e;
        }

        public void ViewLoaded()
        {
            model.RefreshCurrentFlight();
        }

        public void RefreshCurrentFlight()
        {
            model.RefreshCurrentFlight();
        }

        public void ResetCurrentFlight()
        {
            model.ResetCurrentFlight();
        }
    }
}
