﻿using TapVTools.Models.Interfaces;
using TapVTools.Views.Interfaces;

namespace TapVTools.Presenters
{
    public class SignInPresenter
    {
        private readonly ISignInView view;
        private readonly ISignInModel model;

        public SignInPresenter(ISignInView view, ISignInModel model)
        {
            this.view = view;
            this.model = model;
        }

        public void SignIn()
        {
            model.SignIn(new SignInSettings(
                view.Callsign,
                view.Password,
                view.Remember));
        }

        public void ViewLoaded()
        {
            SignInSettings savedSignInSettings = model.GetSavedSignInSettings();
            view.Callsign = savedSignInSettings.Callsign;
            view.Password = savedSignInSettings.Password;
            view.Remember = savedSignInSettings.Remember;

            if (savedSignInSettings.Remember)
                SignIn();
        }
    }
}
