﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TapVTools.Core.Entities;
using TapVTools.Models.Interfaces;
using TapVTools.Views.Interfaces;

namespace TapVTools.Presenters
{
    public class ExamQuestionsPresenter
    {
        private readonly IExamQuestionsView view;
        private readonly IExamQuestionsModel model;

        public ExamQuestionsPresenter(
            IExamQuestionsView view,
            IExamQuestionsModel model)
        {
            model.OnQuestionsChanged += Model_OnQuestionsChanged;

            this.view = view;
            this.model = model;
        }

        private void Model_OnQuestionsChanged(object sender, List<ExamQuestion> e)
        {
            view.Questions = e;
        }

        public Task ViewLoaded()
        {
            return model.GetQuestions();
        }

        public Task Refresh()
        {
            return model.GetQuestions();
        }
    }
}
