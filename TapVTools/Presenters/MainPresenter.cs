﻿using TapVTools.Legacy;
using TapVTools.Models.Interfaces;
using TapVTools.Views;
using TapVTools.Views.Interfaces;

namespace TapVTools.Presenters
{
    public class MainPresenter
    {
        public readonly IMainView view;
        public readonly IMainModel model;

        public MainPresenter(IMainView view, IMainModel model)
        {
            this.view = view;
            this.model = model;
        }

        public CurrentFlightForm CurrentFlightForm
        { get { return model.CurrentFlightForm; } }

        public ExamQuestionsForm ExamQuestionsForm
        { get { return model.ExamQuestionsForm; } }

        public FlightMonitorForm FlightMonitorForm
        { get { return model.FlightMonitorForm; } }

        public string FosUrl { get { return model.FosUrl; } }

        internal void ViewLoaded(string productVersion)
        {
#if DEBUG
            view.SoftwareVersion = string.Format("{0} Development",
                productVersion);
#elif CANARY
            view.SoftwareVersion = string.Format("{0} Canary",
                productVersion);
#else
            view.SoftwareVersion = productVersion;
#endif
        }

        public string FlyTapvWebsiteUrl { get { return model.FlyTapvWebsiteUrl; } }

        public string BugReportUrl {  get { return model.BugReportUrl; } }

        public string FeatureRequestUrl {  get { return model.FeatureRequestUrl; } }

        public void OpenTapLog() { model.OpenTapLog(); }
    }
}
