﻿using Autofac;
using NLog;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using TapVTools.Core;
using TapVTools.Properties;

namespace TapVTools
{
    static class Program
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        [DllImport("user32.dll")]
        private static extern bool SetForegroundWindow(IntPtr hWnd);

        [DllImport("user32.dll")]
        private static extern bool ShowWindowAsync(IntPtr hWnd, int nCmdShow);

        [DllImport("user32.dll")]
        private static extern bool IsIconic(IntPtr hWnd);

        private const int WINDOW_RESTORE = 9;

        private static IContainer appContainer;

        private static AppRouter app;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            if (TryFocusRunningInstance())
                return;

            BuildAppContainer();
            BuildApp();
            UpgradeSettings();

            if (!VerfiyTapLogInstallFolder())
                return;

            Application.Run(app);
        }

        private static bool VerfiyTapLogInstallFolder()
        {
            bool tapLogExists = File.Exists(Path.Combine(
                Settings.Default.TAPLogInstallFolder, "TAPLog.exe"));
            if (!tapLogExists)
            {
                OpenFileDialog dialog = new OpenFileDialog();
                dialog.Title = "Please locate TAPLog.exe";
                dialog.Filter = "TAPLog.exe|TAPLog.exe";
                DialogResult res = dialog.ShowDialog();
                if (res != DialogResult.Cancel)
                {
                    Settings.Default.TAPLogInstallFolder =
                        new FileInfo(dialog.FileName).DirectoryName;
                    Settings.Default.Save();
                }
                else
                    return false;
            }
            return true;
        }

        private static void UpgradeSettings()
        {
            if (Settings.Default.IsFirstRun)
            {
                Settings.Default.Upgrade();
                Settings.Default.IsFirstRun = false;
                Settings.Default.Save();
            }
        }

        private static void BuildAppContainer()
        {
            ContainerBuilder builder = new ContainerBuilder();
            var applicationModule = new ApplicationModule();
            var coreModule = new CoreModule();
#if RELEASE || CANARY
            var infrastrucutreModule =
                new AsaVirtualInfrastructure.AsaVirtualInfrastructureModule();
#else
            var infrastrucutreModule =
                new MemoryInfrastructure.MemoryInfrastructureModule();
#endif

            builder.RegisterModule(coreModule);
            builder.RegisterModule(infrastrucutreModule);
            builder.RegisterModule(applicationModule);

            appContainer = builder.Build();
        }

        private static void BuildApp()
        {
            Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
            Application.ThreadException += Application_ThreadException;
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            app = appContainer.Resolve<AppRouter>();
        }

        private static bool TryFocusRunningInstance()
        {
            int currentPId = Process.GetCurrentProcess().Id;
            Process runningInstance = Process
                .GetProcessesByName(Application.ProductName)
                .Where(p => p.Id != currentPId)
                .FirstOrDefault();

            if (runningInstance == null)
                return false;

            IntPtr hWnd = runningInstance.MainWindowHandle;

            if (IsIconic(hWnd))
                ShowWindowAsync(hWnd, WINDOW_RESTORE);
            SetForegroundWindow(hWnd);

            return true;
        }

        private static void Application_ThreadException(
            object sender,
            ThreadExceptionEventArgs e)
        {
            CloseForUnhandledError(e.Exception);
        }

        private static void CurrentDomain_UnhandledException(
            object sender,
            UnhandledExceptionEventArgs e)
        {
            CloseForUnhandledError((Exception)e.ExceptionObject);
        }

        private static void CloseForUnhandledError(Exception error)
        {
            Logger.Error(error);

            DialogResult reportError = MessageBox.Show(
                @"
You are in the middle of an ocean.

Don’t know where to go.

Don’t know what to do.

The best option is to leave a message and sink, may be someone will find you.

This application is sinking, do wish to leave a message to the developer?
",
                "A fatal error occurred.",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Error);

            if (reportError == DialogResult.Yes)
                Process.Start(@"https://gitlab.com/prodrigues1990/tapv-tools/issues/new?issuable_template=Error");

            Application.Exit();
        }
    }
}
