﻿using AutoMapper;

namespace AsaVirtualInfrastructure.Models
{
    public interface IModelMapper : IMapper { }
}
