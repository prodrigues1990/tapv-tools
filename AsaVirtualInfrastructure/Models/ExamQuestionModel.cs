﻿namespace AsaVirtualInfrastructure.Models
{
    public class ExamQuestionModel
    {
        public string Callsign { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        public bool CorrectedAnswerSelected { get; set; }
    }
}
