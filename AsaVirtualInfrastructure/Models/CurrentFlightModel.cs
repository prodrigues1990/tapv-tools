﻿namespace AsaVirtualInfrastructure.Models
{
    internal class CurrentFlightModel
    {
        public string Callsign { get; set; }

        public string AircraftRegistration { get; set; }

        public string DepartureICAO { get; set; }

        public string ArrivalICAO { get; set; }
    }
}
