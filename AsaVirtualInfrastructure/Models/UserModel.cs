﻿namespace AsaVirtualInfrastructure.Models
{
    internal class UserModel
    {
        public string Callsign { get; set; }

        public string Password { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}
