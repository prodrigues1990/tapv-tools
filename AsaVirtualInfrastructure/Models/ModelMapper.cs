﻿using AutoMapper;

namespace AsaVirtualInfrastructure.Models
{
    internal class ModelMapper : Mapper, IModelMapper
    {
        public ModelMapper() : base(GetConfiguration()) { }

        public static IConfigurationProvider GetConfiguration()
        {
            return new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<ModelMapping>();
            });
        }
    }
}
