﻿using AutoMapper;
using MySql.Data.MySqlClient;
using TapVTools.Core.Entities;

namespace AsaVirtualInfrastructure.Models
{
    public class ModelMapping : Profile
    {
        public ModelMapping()
        {
            CreateMap<UserModel, User>()
                    .ForMember(dest => dest.FullName, opt => opt.MapFrom(src => string.Format("{0} {1}", src.FirstName, src.LastName)));
            CreateMap<MySqlDataReader, UserModel>()
                .ForMember(dest => dest.Callsign, opt => opt.MapFrom(src => src.GetString("callsign")))
                .ForMember(dest => dest.Password, opt => opt.MapFrom(src => src.GetString("pass")))
                .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.GetString("fst_name")))
                .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.GetString("lst_name")));

            CreateMap<CurrentFlightModel, CurrentFlight>()
                .ForMember(dest => dest.AircraftRegistration, opt => opt.MapFrom(src => src.AircraftRegistration))
                .ForMember(dest => dest.DepartureICAO, opt => opt.MapFrom(src => src.DepartureICAO))
                .ForMember(dest => dest.ArrivalICAO, opt => opt.MapFrom(src => src.ArrivalICAO));
            CreateMap<MySqlDataReader, CurrentFlightModel>()
                .ForMember(dest => dest.Callsign, opt => opt.MapFrom(src => src.GetString("callsign")))
                .ForMember(dest => dest.AircraftRegistration, opt => opt.MapFrom(src => src.GetString("aircraft_registration")))
                .ForMember(dest => dest.DepartureICAO, opt => opt.MapFrom(src => src.GetString("ICAO_ori")))
                .ForMember(dest => dest.ArrivalICAO, opt => opt.MapFrom(src => src.GetString("ICAO_dest")));

            CreateMap<ExamQuestionModel, ExamQuestion>()
                .ForMember(dest => dest.Question,
                            opt => opt.MapFrom(src => src.Question))
                .ForMember(dest => dest.Answer,
                            opt => opt.MapFrom(src => src.Answer))
                .ForMember(dest => dest.CorrectedAnswerSelected,
                            opt => opt.MapFrom(src => src.CorrectedAnswerSelected));
            CreateMap<MySqlDataReader, ExamQuestionModel>()
                .ForMember(dest => dest.Callsign,
                            opt => opt.MapFrom(src => src.GetString("lst_user_change")))
                .ForMember(dest => dest.Question,
                            opt => opt.MapFrom(src => src.GetString("question_desc")))
                .ForMember(dest => dest.Answer,
                            opt => opt.MapFrom(src => src.GetString("answers_desc")))
                .ForMember(dest => dest.CorrectedAnswerSelected,
                            opt => opt.MapFrom(src => src.GetBoolean("anws_true_false")));
        }
    }
}
