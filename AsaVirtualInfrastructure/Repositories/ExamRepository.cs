﻿using AsaVirtualInfrastructure.Models;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Threading.Tasks;
using TapVTools.Core.Entities;
using TapVTools.Core.Interfaces.Repositories;

namespace AsaVirtualInfrastructure.Repositories
{
    public class ExamRepository : IExamRepository
    {
        private IMySqlProvider dbProvider;
        private IModelMapper mapper;
        private UserRepository userRepository;

        public ExamRepository(
            IMySqlProvider dbProvider,
            IModelMapper mapper,
            UserRepository userRepository)
        {
            this.dbProvider = dbProvider;
            this.mapper = mapper;
            this.userRepository = userRepository;
        }

        public async Task<List<ExamQuestion>> GetCurrentExamQuestions()
        {
            UserModel user = userRepository.currentUser;
            List<ExamQuestionModel> models = new List<ExamQuestionModel>();
            using (MySqlConnection conn = dbProvider.getConnection())
            {
                MySqlCommand getExamQuestion = new MySqlCommand(
                    @"
SELECT
    tbl_exams_requests.lst_user_change,
    tbl_exam_questions.question_desc,
    tbl_exams_answers.answers_desc,
    IF(tbl_exams_requests.anws_true_false IS NULL, 0,
        tbl_exams_requests.anws_true_false) as anws_true_false
FROM tbl_exams_requests
INNER JOIN tbl_exam_questions ON tbl_exam_questions.question_id=tbl_exams_requests.question_id_FK
INNER JOIN tbl_exams_answers ON tbl_exams_answers.question_id_FK=tbl_exam_questions.question_id
WHERE tbl_exams_requests.lst_dte_change > DATE_SUB(NOW(), INTERVAL 74 HOUR) AND tbl_exams_answers.value = 1
ORDER BY tbl_exams_requests.lst_user_change, tbl_exams_requests.request_id_FK, tbl_exams_requests.question_nmbr;",
                    conn);
                try
                {
                    await conn.OpenAsync();
                    var reader = await getExamQuestion.ExecuteReaderAsync();
                    while (reader.Read())
                    {
                        ExamQuestionModel examQuestionModel =
                            mapper.Map<ExamQuestionModel>(reader);
                        if (examQuestionModel.Callsign == user.Callsign)
                            models.Add(examQuestionModel);
                    }
                }
                finally { conn.Close(); }
            }

            return mapper.Map<List<ExamQuestion>>(models);
        }
    }
}
