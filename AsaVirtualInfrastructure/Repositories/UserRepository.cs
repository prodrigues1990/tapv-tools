﻿using AsaVirtualInfrastructure.Models;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Threading.Tasks;
using TapVTools.Core.Entities;
using TapVTools.Core.Interfaces.Repositories;

namespace AsaVirtualInfrastructure.Repositories
{
    public class UserRepository : IUserRepository
    {
        private UserModel _currentUser;

        internal UserModel currentUser
        {
            get => _currentUser == null ? throw new NotSignedIn() : _currentUser;
            private set => _currentUser = value;
        }

        private IMySqlProvider dbProvider;

        private IModelMapper mapper;

        private Dictionary<string, UserModel> userModels;

        public UserRepository(IMySqlProvider dbProvider, IModelMapper mapper)
        {
            this.dbProvider = dbProvider;
            this.mapper = mapper;
            userModels = new Dictionary<string, UserModel>();
        }

        public Task<User> GetCurrentUser()
        {
            return Task.FromResult(mapper.Map<User>(currentUser));
        }

        public async Task SignIn(string callsign, string password)
        {
            userModels = await getUserModels();

            try
            {
                UserModel userModel = userModels[callsign];
                bool passwordIsValid = userModel.Password == password;
                if (passwordIsValid)
                    currentUser = userModel;
                else
                    throw new WrongCredentials();
            }
            catch (KeyNotFoundException)
            {
                throw new WrongCredentials();
            }
        }

        private async Task<Dictionary<string, UserModel>> getUserModels()
        {
            Dictionary<string, UserModel> userModels = new Dictionary<string, UserModel>();
            using (MySqlConnection conn = dbProvider.getConnection())
            {
                MySqlCommand getUsersCommand = new MySqlCommand(
                        "SELECT callsign, pass, fst_name, lst_name FROM tbl_users;",
                        conn);
                try
                {
                    await conn.OpenAsync();
                    var userReader = await getUsersCommand.ExecuteReaderAsync();
                    while (userReader.Read())
                    {
                        UserModel userModel = mapper.Map<UserModel>(userReader);
                        userModels.Add(userModel.Callsign, userModel);
                    }
                }
                finally { conn.Close(); }
            }
            return userModels;
        }
    }
}
