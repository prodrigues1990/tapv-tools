﻿using AsaVirtualInfrastructure.Models;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Threading.Tasks;
using TapVTools.Core.Entities;
using TapVTools.Core.Interfaces.Repositories;

namespace AsaVirtualInfrastructure.Repositories
{
    public class FlightRepository : IFlightRepository
    {
        private IMySqlProvider dbProvider;
        private IModelMapper mapper;
        private UserRepository userRepository;

        public FlightRepository(
            IMySqlProvider dbProvider,
            IModelMapper mapper,
            UserRepository userRepository)
        {
            this.dbProvider = dbProvider;
            this.mapper = mapper;
            this.userRepository = userRepository;
        }

        public async Task<CurrentFlight> GetCurrentFlight()
        {
            UserModel user = userRepository.currentUser;
            Dictionary<string, CurrentFlightModel> models
                = new Dictionary<string, CurrentFlightModel>();
            using (MySqlConnection conn = dbProvider.getConnection())
            {
                MySqlCommand getAssignedFlightsCommand = new MySqlCommand(
                    @"
SELECT tbl_users.callsign, tbl_hangar.aircraft_registration, tbl_users.ICAO_ori, tbl_users.ICAO_dest
FROM tbl_hangar
INNER JOIN tbl_users ON aircraft_registration_FK = aircraft_registration
WHERE lst_date_location_change IS NOT NULL AND ICAO_ori IS NOT NULL AND ICAO_dest IS NOT NULL; ",
                    conn);
                try
                {
                    await conn.OpenAsync();
                    var reader = await getAssignedFlightsCommand.ExecuteReaderAsync();
                    while (reader.Read())
                    {
                        CurrentFlightModel currentFlightModel =
                            mapper.Map<CurrentFlightModel>(reader);
                        models.Add(currentFlightModel.Callsign, currentFlightModel);
                    }
                }
                finally { conn.Close(); }
            }

            try
            {
                return mapper.Map<CurrentFlight>(models[user.Callsign]);
            }
            catch (KeyNotFoundException error)
            {
                throw new NoFlightAssigned();
            }
        }

        public async Task ResetCurrentFlight()
        {
            CurrentFlight currentFlight = await GetCurrentFlight();

            using (MySqlConnection conn = dbProvider.getConnection())
            {
                MySqlCommand resetAssignedFlightsCommand = new MySqlCommand(
                    "UPDATE tbl_hangar SET lst_date_location_change = DATE_SUB(lst_date_location_change, INTERVAL 65 MINUTE) WHERE aircraft_registration = @Registration;",
                    conn);
                resetAssignedFlightsCommand.Parameters.AddWithValue(
                    "@Registration", currentFlight.AircraftRegistration);
                await conn.OpenAsync();
                var reader = await resetAssignedFlightsCommand.ExecuteNonQueryAsync();
            }

            return;
        }
    }
}
