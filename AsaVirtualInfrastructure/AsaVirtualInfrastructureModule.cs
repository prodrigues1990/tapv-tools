﻿using AsaVirtualInfrastructure.Models;
using AsaVirtualInfrastructure.Repositories;
using Autofac;
using TapVTools.Core.Interfaces.Repositories;

namespace AsaVirtualInfrastructure
{
    public class AsaVirtualInfrastructureModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ModelMapper>()
                .As<IModelMapper>()
                .SingleInstance();

            builder.RegisterType<MySqlProvider>()
                .As<IMySqlProvider>()
                .InstancePerLifetimeScope();

            builder.RegisterType<UserRepository>()
                .As<IUserRepository>()
                .As<UserRepository>()
                .SingleInstance();
            builder.RegisterType<FlightRepository>()
                .As<IFlightRepository>()
                .InstancePerLifetimeScope();
            builder.RegisterType<ExamRepository>()
                .As<IExamRepository>()
                .SingleInstance();
        }
    }
}
